const favouritesMovies = {
    "Matrix": {
        imdbRating: 8.3,
        actors: ["Keanu Reeves", "Carrie-Anniee"],
        oscarNominations: 2,
        genre: ["sci-fi", "adventure"],
        totalEarnings: "$680M"
    },
    "FightClub": {
        imdbRating: 8.8,
        actors: ["Edward Norton", "Brad Pitt"],
        oscarNominations: 6,
        genre: ["thriller", "drama"],
        totalEarnings: "$350M"
    },
    "Inception": {
        imdbRating: 8.3,
        actors: ["Tom Hardy", "Leonardo Dicaprio"],
        oscarNominations: 12,
        genre: ["sci-fi", "adventure"],
        totalEarnings: "$870M"
    },
    "The Dark Knight": {
        imdbRating: 8.9,
        actors: ["Christian Bale", "Heath Ledger"],
        oscarNominations: 12,
        genre: ["thriller"],
        totalEarnings: "$744M"
    },
    "Pulp Fiction": {
        imdbRating: 8.3,
        actors: ["Sameul L. Jackson", "Bruce Willis"],
        oscarNominations: 7,
        genre: ["drama", "crime"],
        totalEarnings: "$455M"
    },
    "Titanic": {
        imdbRating: 8.3,
        actors: ["Leonardo Dicaprio", "Kate Winslet"],
        oscarNominations: 13,
        genre: ["drama"],
        totalEarnings: "$800M"
    }
}


// NOTE: For all questions, the returned data must contain all the movie information including its name.

// Q1. Find all the movies with total earnings more than $500M. 
let movieEarnings = Object.fromEntries(Object.entries(favouritesMovies).filter((movies) => {
    let value = movies[1].totalEarnings;
    let earning = "$500M";
    let earningValue = parseInt(earning.replace('$', ''));
    return parseInt(value.replace('$', '')) > earningValue;
}).map((names) => {
    return names;
}));

//console.log(movieEarnings);


// Q2. Find all the movies who got more than 3 oscarNominations and also totalEarning are more than $500M.
let moviesOscarNominations =Object.fromEntries(Object.entries(movieEarnings).filter((oscar)=> {
    if(oscar[1].oscarNominations>3){
        return oscar;
    }
}));
console.log(moviesOscarNominations);


// Q.3 Find all movies of the actor "Leonardo Dicaprio".
let leonardoActorMovies = Object.fromEntries(Object.entries(favouritesMovies).filter((actorMovies) => {
    if (actorMovies[1].actors.includes("Leonardo Dicaprio")) {
        return actorMovies;
    }
}).map((movie) => {
    return movie;
}));

console.log(leonardoActorMovies);

// Q.4 Sort movies (based on IMDB rating)
//  if IMDB ratings are same, compare totalEarning as the secondary metric.

let moviesImdbRating = Object.fromEntries(Object.entries(favouritesMovies).sort((first, second) => {
    if (first[1].imdbRating - second[1].imdbRating != 0) {
        return first[1].imdbRating - second[1].imdbRating;
    }
    else {
        let firstValue = parseInt(first[1].totalEarnings.replace('$', ''));
        let secondValue = parseInt(second[1].totalEarnings.replace('$', ''));
        return firstValue - secondValue;
    }
}));
console.log(moviesImdbRating);

// Q.5 Group movies based on genre. Priority of genres in case of multiple genres present are:
//     drama > sci-fi > adventure > thriller > crime

/*let moviesOnGenre = Object.entries(favouritesMovies).reduce((accumulater, currentValue) => {
    if (accumulater[currentValue[1].genre]) {
        if (accumulater[currentValue[1].genre.includes("drama")]) {
            accumulater[currentValue[0]] = currentValue[1];
        }
    }
    else{
        if (accumulater[currentValue[1].genre.includes("drama")]){
            accumulater[currentValue[0]]=currentValue[1];
        }
        
    }
    if (accumulater[currentValue[1].genre.includes("sci-fi")]) {
        accumulater[currentValue[0]] = currentValue[1];
    }
    if (accumulater[currentValue[1].genre.includes("adventure")]) {
        accumulater[currentValue[0]] = currentValue[1];
    }
    if (accumulater[currentValue[1].genre.includes("thriller")]) {
        accumulater[currentValue[0]] = currentValue[1];
    }
    if (accumulater[currentValue[1].genre.includes("crime")]) {
        accumulater[currentValue[0]] = currentValue[1];
    }
    return accumulater;
});
console.log(moviesOnGenre);
  */
